//---------------------------------------------------------------------------
#ifndef MATRIX_H
  #define MATRIX_H
#ifndef __STDIO_H
   #include <iostream.h>
   #endif
//  #if !(defined(__COMPLEX_H) || defined(_STLP_template_complex))
    #include <complex.h>
//    #endif
//---------------------------------------------------------------------------
#pragma package(smart_init)
#define meWRONG_INDEX 0
#define meUNDEFINED_VALUE 1
#define meZERO_DETERMINANT 2

class MatrixException: std::exception {
//   private:
public:
      int Type;
      double Value;
   public:
      MatrixException(int Error, double Data) {Type=Error; Value=Data;};
      MatrixException(int Error) {Type=Error; Value=0;};
      virtual const char* what () const throw();
   };

template <class Element> class Matrix {
  protected:
   Element *Table;
   unsigned FRows, FCols;
  public:
   unsigned Rows() const;
   unsigned Cols() const;
   Matrix (unsigned r, unsigned c) throw (MatrixException); //������� ������� � r ����� � c ��������
   Matrix (const Matrix& m);
   ~Matrix();
   Element& operator() (unsigned Row,unsigned Col) throw (MatrixException);  //������ ���
                //��������� � ��������� �������
   Matrix operator+(Matrix op2) throw (MatrixException);
   Matrix operator+=(Matrix op2) throw (MatrixException);
   Matrix operator-(Matrix op2) throw (MatrixException);
   Matrix operator-=(Matrix op2) throw (MatrixException);
   Matrix operator*(Matrix op2) throw (MatrixException);  //��������� ���������
   bool operator==(Matrix op2);
   bool operator!=(Matrix op2);
   Matrix operator*(long double op2) throw (MatrixException);  //��������� ���������
   Matrix operator*(complex<long double> op2) throw (MatrixException);  //��������� ���������
   Matrix operator*=(long double op2) throw (MatrixException);  //��������� ���������
   Matrix operator*=(complex<long double> op2) throw (MatrixException);  //��������� ���������
   virtual int operator=(int op2) throw (MatrixException); //������ ��� ������� �������
                //� ��������� ������
   const Matrix& operator=(Matrix op2) throw (MatrixException); //���������� �����������
//   const Matrix& operator=(Element** op2); //����������� ���������� ������ � �������
   Matrix T(); //���������� ����������������� �������
   bool ChangeRows(unsigned n1, unsigned n2); //������ ������� ��� ������
   bool ChangeColumns(unsigned n1, unsigned n2); //������ ������� ��� �������
   void Insert(Matrix &op2, unsigned R, unsigned C) throw (MatrixException); //��������� ������ ������� � ������, ������� � ��������� �������
   Matrix Submatrix(unsigned rb, unsigned cb, unsigned re, unsigned ce) throw (MatrixException); //�������� �� ������ ������� ����� � ���������� ����� ������� � ������ ������ ������
   int Trianglize(); //�������� ������� � ����������� �����. ���������� 1, ���� ����� ������������ ����� ������ � -1, ���� ��������
//   void ToArray(Element* A); //��������� �������� ������� � ���������� ������
   };

template <class Element> ostream& operator<<(ostream &s, Matrix<Element> m);
template <class Element> Matrix<Element> operator*(long double n, Matrix<Element> m)
         {return m*n;};
//template <class Element> Matrix<Element> operator*(complex<long double> n, Matrix<Element> m)
//         {return m*n;};

template <class Element> class SquareMatrix: public Matrix<Element> {
  public:
   SquareMatrix(unsigned Size) throw (MatrixException): Matrix<Element>(Size,Size){};
   SquareMatrix(const Matrix<Element>& m) throw (MatrixException);
   virtual int operator=(int op2) throw (MatrixException); //������ ��� ������� �������
                //� ��������� ������
   const SquareMatrix& operator=(Matrix<Element> op2) throw (MatrixException)
       {Matrix<Element>::operator=(op2); return *this;};
   Element Determinant();
   Element Det();
   SquareMatrix Inv() throw (MatrixException);
   };

template <class Element> class Vector: public Matrix<Element> {
  public:
   Vector(unsigned Size) throw (MatrixException): Matrix<Element>(Size,1){};
   Vector(const Matrix<Element>& m) throw (MatrixException);
   Element& operator()(unsigned i) throw (MatrixException);
   };

#endif
 