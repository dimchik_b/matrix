//---------------------------------------------------------------------------
#pragma hdrstop
#include <exception>
#ifndef __STDIO_H
   #include <iostream.h>
   #endif
#if !(defined(__COMPLEX_H) || defined(_STLP_template_complex))
   #include <complex.h>
   #endif

#include "Matrix.h"

//---------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////////////////
//----------------------------Matrix--------------------------------------------
template <class Element> Matrix<Element>::Matrix (unsigned r, unsigned c)
        throw (MatrixException){
   if (!(r && c)) throw (MatrixException(meWRONG_INDEX,0));
   FRows=r; FCols=c;
   Table=new Element[FRows*FCols];
   };
template <class Element> Matrix<Element>::Matrix (const Matrix& m){
   FRows=m.FRows; FCols=m.FCols;
   Table=new Element[FRows*FCols];
   unsigned n=FRows*FCols;
   for (unsigned i=0; i<n; i++)
      Table[i]=m.Table[i];
   };
template <class Element> Matrix<Element>::~Matrix (){
   delete [] Table;
   };
template <class Element> unsigned Matrix<Element>::Rows() const{
   return FRows;
   };
template <class Element> unsigned Matrix<Element>::Cols()const{
   return FCols;
   };
template <class Element> Element& Matrix<Element>::operator() (unsigned Row,unsigned Col) throw (MatrixException){
   if (!(Row && Col)) throw (MatrixException(meWRONG_INDEX,0));
   if (Row>FRows) throw (MatrixException(meWRONG_INDEX, Row));
   if (Col>FCols) throw (MatrixException(meWRONG_INDEX, Col));
   return *(Table + FCols*--Row + --Col);
   };
template <class Element> Matrix<Element> Matrix<Element>::operator+(Matrix op2) throw (MatrixException){
   if (FRows!=op2.FRows) throw (MatrixException(meWRONG_INDEX, op2.FRows));
   if (FCols!=op2.FCols) throw (MatrixException(meWRONG_INDEX, op2.FCols));
   Matrix<Element> res(FRows, FCols);
   unsigned n=FRows*FCols;
   for (unsigned i=0; i<n; i++)
      res.Table[i]=Table[i]+op2.Table[i];
   return res;
   };
template <class Element> Matrix<Element> Matrix<Element>::operator+=(Matrix op2) throw (MatrixException){
   if (FRows!=op2.FRows) throw (MatrixException(meWRONG_INDEX, op2.FRows));
   if (FCols!=op2.FCols) throw (MatrixException(meWRONG_INDEX, op2.FCols));
   Matrix<Element> res(FRows, FCols);
   unsigned n=FRows*FCols;
   for (unsigned i=0; i<n; i++)
      Table[i]=res.Table[i]=Table[i]+op2.Table[i];
   return res;
   };
template <class Element> Matrix<Element> Matrix<Element>::operator-(Matrix op2) throw (MatrixException){
   if (FRows!=op2.FRows) throw (MatrixException(meWRONG_INDEX, op2.FRows));
   if (FCols!=op2.FCols) throw (MatrixException(meWRONG_INDEX, op2.FCols));
   Matrix<Element> res(FRows, FCols);
   unsigned n=FRows*FCols;
   for (unsigned i=0; i<n; i++)
      res.Table[i]=Table[i]-op2.Table[i];
   return res;
   };
template <class Element> Matrix<Element> Matrix<Element>::operator-=(Matrix op2) throw (MatrixException){
   if (FRows!=op2.FRows) throw (MatrixException(meWRONG_INDEX, op2.FRows));
   if (FCols!=op2.FCols) throw (MatrixException(meWRONG_INDEX, op2.FCols));
   Matrix<Element> res(FRows, FCols);
   unsigned n=FRows*FCols;
   for (unsigned i=0; i<n; i++)
      Table[i]=res.Table[i]=Table[i]-op2.Table[i];
   return res;
   };
template <class Element> Matrix<Element> Matrix<Element>::operator*(Matrix op2) throw (MatrixException){
   if (FCols!=op2.FRows) throw (MatrixException(meWRONG_INDEX, op2.FRows));
   Matrix<Element> res(FRows, op2.FCols), &m=*this;
   for (unsigned r=1; r<=FRows; r++)
      for (unsigned c=1; c<=op2.FCols; c++){
         Element s=0;
         for (unsigned i=1; i<=FCols; i++)
            s=s+m(r,i)*op2(i,c);
         res(r,c)=s;
         };
   return res;
   };
template <class Element> bool Matrix<Element>::operator==(Matrix op2){
   unsigned n=FRows*FCols;
   for (unsigned i=0; i<n; i++)
      if(Table[i]!=op2.Table[i]) return false;
   return true;
   };
template <class Element> bool Matrix<Element>::operator!=(Matrix op2){
   return !operator==(op2);
   };
template <class Element> Matrix<Element> Matrix<Element>::operator*(long double op2) throw (MatrixException){
   Matrix<Element> res(FRows, FCols);
   unsigned n=FRows*FCols;
   for (unsigned i=0; i<n; i++)
      res.Table[i]=op2*Table[i];
   return res;
   };
template <class Element> Matrix<Element> Matrix<Element>::operator*(complex<long double> op2) throw (MatrixException){
   Matrix<Element> res(FRows, FCols);
   unsigned n=FRows*FCols;
   for (unsigned i=0; i<n; i++)
      res.Table[i]=op2*Table[i];
   return res;
   };
template <class Element> Matrix<Element> Matrix<Element>::operator*=(complex<long double> op2) throw (MatrixException){
   Matrix<Element> res(FRows, FCols);
   unsigned n=FRows*FCols;
   for (unsigned i=0; i<n; i++)
      Table[i]=res.Table[i]=op2*Table[i];
   return res;
   };
template <class Element> Matrix<Element> Matrix<Element>::operator*=(long double op2) throw (MatrixException){
   Matrix<Element> res(FRows, FCols);
   unsigned n=FRows*FCols;
   for (unsigned i=0; i<n; i++)
      Table[i]=res.Table[i]=op2*Table[i];
   return res;
   };
template <class Element> const Matrix<Element>& Matrix<Element>::operator=(Matrix op2) throw (MatrixException){
   if (FRows!=op2.FRows ||
        FCols!=op2.FCols) throw (MatrixException(meWRONG_INDEX,0));
   unsigned n=FRows*FCols;
   for (unsigned i=0; i<n; i++)
      Table[i]=op2.Table[i];
   return *this;
   };
/*template <class Element> const Matrix<Element>& Matrix<Element>::operator=(Element** op2) {
   for (unsigned r=0; r<FRows; r++)
      for (unsigned c=0; c<FCols; c++)
         Table[r*FCols+c]=op2[r][c];
   return *this;
   };          */
template <class Element> int Matrix<Element>::operator=(int op2) throw (MatrixException){
   switch (op2) {
      case 0:{
         unsigned n=FRows*FCols;
         for (unsigned i=0; i<n; i++)
            Table[i]=0;
         return 0;}
      default:
         throw (MatrixException(meUNDEFINED_VALUE, op2));
      };
   };
template <class Element> Matrix<Element> Matrix<Element>::T(){
   Matrix<Element> res(FCols, FRows), &m=*this;
   for (unsigned r=1; r<=FRows; r++)
      for (unsigned c=1; c<=FCols; c++)
         res(c,r)=m(r,c);
   return res;
   };
template <class Element> bool Matrix<Element>::ChangeRows(unsigned n1, unsigned n2){
   if (n1>FRows) throw (MatrixException(meWRONG_INDEX, n1));
   if (n2>FRows) throw (MatrixException(meWRONG_INDEX, n2));
   if (n1==n2) return false;
   Matrix<Element> temp(1, FCols);
   for (unsigned i=1; i<=FCols; i++)
      temp(1,i)=operator()(n1,i);
   for (unsigned i=1; i<=FCols; i++)
      operator()(n1,i)=operator()(n2,i);
   for (unsigned i=1; i<=FCols; i++)
      operator()(n2,i)=temp(1,i);
   return true;
   };
template <class Element> bool Matrix<Element>::ChangeColumns(unsigned n1, unsigned n2){
   if (n1>FCols) throw (MatrixException(meWRONG_INDEX, n1));
   if (n2>FCols) throw (MatrixException(meWRONG_INDEX, n2));
   if (n1==n2) return false;
   Matrix<Element> temp(FRows,1);
   for (unsigned i=1; i<=FRows; i++)
      temp(i,1)=operator()(i,n1);
   for (unsigned i=1; i<=FRows; i++)
      operator()(i,n1)=operator()(i,n2);
   for (unsigned i=1; i<=FRows; i++)
      operator()(i,n2)=temp(i,1);
   return true;
   };
template <class Element> void Matrix<Element>::Insert(Matrix &op2, unsigned R, unsigned C) throw (MatrixException){
   if (op2.FRows+R-1>FRows) throw (MatrixException(meWRONG_INDEX, op2.FRows+R-1));
   if (op2.FCols+C-1>FCols) throw (MatrixException(meWRONG_INDEX, op2.FCols+C-1));
   for (unsigned r=1;r<=op2.FRows; r++)
       for (unsigned c=1;c<=op2.FCols; c++)
           operator()(R+r-1,C+c-1)=op2(r,c);
   };
template <class Element> Matrix<Element> Matrix<Element>::Submatrix(unsigned rb, unsigned cb, unsigned re, unsigned ce) throw (MatrixException){
   if (re>FRows) throw (MatrixException(meWRONG_INDEX, re));
   if (rb>re) throw (MatrixException(meWRONG_INDEX, rb));
   if (ce>FCols) throw (MatrixException(meWRONG_INDEX, ce));
   if (cb>ce) throw (MatrixException(meWRONG_INDEX, cb));
   Matrix<Element> m(re-rb+1,ce-cb+1);
   for (unsigned r=1;r<=m.FRows; r++)
       for (unsigned c=1;c<=m.FCols; c++)
           m(r,c)=operator()(rb+r-1,cb+c-1);
   return m;
   };
template <class Element> int Matrix<Element>::Trianglize(){
   int res=1;
   for (unsigned r=1; r<FRows; r++){
      unsigned n1=r;
      for (; n1<=FRows;){ //�� ���� ����� �� ���� r-�
         if (n1>FCols) return res;
         if (operator()(n1,r)!=0) break; //���� ������, � ������� ���� ��������� �������� � r-� ������� ��� �����
         n1++;
         };
      if (ChangeRows(r,n1)) res=-res; //���� r<>n1, �� ���� ������������
      for (unsigned r1=r+1; r1<=FRows; r1++){ //��� ���� ����� ���� r-���
         if (operator()(r1,r)==0) continue; //���� ������ ��������� ������� � �������
         Element k=operator()(r1,r)/operator()(r,r); //���������� ��������� k
         operator()(r1,r)=0;
         for (unsigned c=r+1; c<=FCols; c++) //� �� ���� ��������� ������ r-��
            operator()(r1,c)-=k*operator()(r,c); //�������� �������� r-� ������, ���������� �� k
         };
      };
   return res;
   };

/*template <class Element> void Matrix<Element>::ToArray(Element* A){
   for (int r=0; r<FRows; r++)
      for(int c=0; c<FCols; c++)
         A[r][c]=Table[r*FCols+c];
   }  */

template <class Element> ostream& operator<<(ostream &s, Matrix<Element> m){
   for (unsigned r=1; r<=m.Rows(); r++){
      for (unsigned c=1; c<=m.Cols(); c++){
        s<<m(r,c)<<'\t';
        };
      s<<'\n';
      };
   return s;
   };

//----------------------SquareMatrixt-------------------------------------------
template <class Element> SquareMatrix<Element>::SquareMatrix (const Matrix<Element>& m) throw (MatrixException)
          : Matrix<Element>(m.Rows(), m.Rows()){
   if(m.Rows()!=m.Cols()) throw (MatrixException(meWRONG_INDEX));
   for (unsigned r=1; r<=FRows; r++)
      for (unsigned c=1; c<=FCols; c++)
         operator()(r,c)=m(r,c);
   };
template <class Element> int SquareMatrix<Element>::operator=(int op2) throw (MatrixException){
   switch (op2) {
      case 1: {
         unsigned n=FRows*FCols;
         for (unsigned i=0; i<n; i++)
           Table[i]=i%(FRows+1)?0:1;
         return 1;
         }
      default:
         return Matrix<Element>::operator=(op2);
      };
   };
template <class Element> Element SquareMatrix<Element>::Determinant(){
   if(FRows==1) return Table[0];
      else {
         SquareMatrix<Element> Minor(FRows-1);
         Element ad, res=0;
         for (unsigned c=1; c<=FCols; c++){
            unsigned rm, cm;
            rm=1;
            for (unsigned rr=2; rr<=FRows; rr++){
               cm=1;
               for (unsigned cc=1; cc<=FCols; cc++){
                  if (cc==c) continue;
                  Minor(rm,cm++)=operator()(rr,cc);
                  };
               rm++;
               };
            ad=operator()(1,c)*Minor.Determinant();
            if (c%2) res+=ad;
               else res-=ad;
            };
         return res;
         };
   };
template <class Element> Element SquareMatrix<Element>::Det(){
   SquareMatrix<Element> m(*this);
   Element res=1;
   // ������������ ���� ������������� � ���������� i
   for (unsigned r=1; r<FRows; r++){
   //    ����� ������ n, ��� �(n,i)<>0
      unsigned n1=r;
      for (; n1<=FRows;){
         if (m(n1,r)!=0) break;
         n1++;
         };
   //    ���� �� �������, ������� 0 � ���������
      if (n1>FRows) return 0;
   //    �������� �� � i-� �������. �������� ��� � ����� ������������
      if(m.ChangeRows(r,n1)) res=-res;
   //    �������� �������� �(...,i) � ������� ���� i
      for (unsigned r1=r+1; r1<=FRows; r1++){
         if (m(r1,r)==0) continue;
         Element k=m(r1,r)/m(r,r);
         m(r1,r)=0;
         for (unsigned c=r+1; c<=FCols; c++)
            m(r1,c)-=k*m(r,c);
         };
      };
   // ����������� �������� ������� ���������
   for (unsigned i=1; i<=FRows; i++)
      res*=m(i,i);
   // ������ ���������
   return res;
   };
template <class Element> SquareMatrix<Element> SquareMatrix<Element>::Inv() throw (MatrixException){
   SquareMatrix<Element> m(*this), res(FRows); //�������� �������� ������� �� ��������� ������� m
   res=1; //������ ������ ���������� ��� ��������� �������
   for (unsigned r=1; r<=FRows; r++){
      unsigned n1=r;
      while (n1<=FRows){ //���� ���� �� ���� ������, � ������� ������� � r-� ������� <>0
         if (m(n1,r)!=0) break;
         n1++;
         };
      if (n1>FRows) throw (MatrixException(meZERO_DETERMINANT)); //���� ����� ���, �� �������� ������� �� ����������
      m.ChangeRows(r,n1); //������ ��� ������ � r-� ������� �� ��������� ������� � � ������� ����������
      res.ChangeRows(r,n1);
      Element k=m(r,r);
      for (unsigned c=r; c<=FCols; c++)
         m(r,c)/=k;       //����� ��� ������� �� ������� m(r,r). (��� �������� ����� ����� - �������)
      m(r,r)=1;
      for (unsigned c=1; c<=r; c++)
         res(r,c)/=k;     //�� ��� �� ����� ����� ����� �� ������� � ������� ���������� (��� �������� ������ (r,r) - �������)
      for (unsigned r1=r+1; r1<=FRows; r1++){
         if (m(r1,r)==0) continue;  //�� ���� ������� ���� �������� ������ � ����� �������������, �����
         Element k=m(r1,r);         //���� ������� ��������� ��� �������� ���� ��������
         for (unsigned c=r; c<=FCols; c++)
            m(r1,c)-=k*m(r,c);
         m(r1,r)=0;
         for (unsigned c=1; c<=r; c++) //����������� ����������� �� �� �������� �� ��������� ������� ����������,
            res(r1,c)-=k*res(r,c);     //��� �� �����������. (���� ������� ��������� ��� �������� �������)
         };
      };
   for (unsigned r=FRows-1; r>0; r--)   //������ ����� �����
      for (unsigned c=FCols; c>r; c--){ //� ������ ������
         Element k=m(r,c);
         for (unsigned i=1; i<=FCols; i++)
            res(r,i)-=k*res(c,i); // �������� � ������� ���������� c-� ������� �� r-� � ������������� ������ m(r,c)
         };
   return res; //� ���������� ���������� �������� �������
   };
//----------------------Vector--------------------------------------------------
template <class Element> Vector<Element>::Vector (const Matrix<Element>& m) throw (MatrixException)
          : Matrix<Element>(m.Rows(), 1){
   if(m.Cols()!=1) throw (MatrixException(meWRONG_INDEX,1));
   for (unsigned r=1; r<=FRows; r++)
      operator()(r)=m(r,1);
   };
template <class Element> Element& Vector<Element>::operator() (unsigned i) throw (MatrixException){
   return Matrix<Element>::operator()(i,1);
   };

//----------------------MatrixtException----------------------------------------
const char* MatrixException::what () const throw(){
   char* r;
   switch (Type){
      case meZERO_DETERMINANT:
         r="������� �����������";
      case meWRONG_INDEX:
         r="������ ������������ �������";
      case meUNDEFINED_VALUE:
         r="�������������� �������� �������� �������";
      default:
         r="����������� ������ ��� ������ � ���������";
      }
   return r;
   };




